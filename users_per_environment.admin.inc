<?php

/**
 * @file
 * The admin settings for the Users per Environment module.
 */

/**
 * Implements hook_settings().
 */
function users_per_environment_admin_settings() {
  $form['#after_build'][] = 'users_per_environment_after_build';
  $form['users_per_environment_skip_environment'] = [
    '#type' => 'checkbox',
    '#title' => t('Skip the environment check.'),
    '#default_value' => variable_get('users_per_environment_skip_environment', 1),
    '#description' => t('Unselect this option after all users in PROD are defined and the database has been cloned back to all environments.'),
  ];
  $form['users_per_environment_site_url'] = [
    '#type' => 'fieldset',
    '#title' => t('Site URL of production'),
    // Make containing fields align horizontally.
    '#attributes' => ['class' => ['container-inline']],
  ];
  $form['users_per_environment_site_url']['users_per_environment_site_url_sld'] = [
    '#type' => 'textfield',
    '#default_value' => variable_get('users_per_environment_site_url_sld', 'example'),
    '#field_prefix' => 'http(s)://(www.)',
    '#field_suffix' => '.',
    '#size' => 20,
    '#required' => TRUE,
  ];
  $form['users_per_environment_site_url']['users_per_environment_site_url_tld'] = [
    '#type' => 'textfield',
    '#default_value' => variable_get('users_per_environment_site_url_tld', 'com'),
    '#description' => '<br />' . t("Use only the first part in case of a second-level domain (e.g. 'co' instead of 'co.uk')."),
    '#field_suffix' => '/',
    '#size' => 5,
    '#maxlength' => 24,
    '#required' => TRUE,
  ];
  $field_settings = l(t('field settings'), 'admin/config/people/accounts/fields/field_users_per_environment', [
    'attributes' => [
      'title' => t("Configure the field 'Environments'."),
    ],
  ]);
  $form['users_per_environment_environments'] = [
    '#markup' => '<p>' . t("Define your environments at the !field_settings in the 'Allowed values list' section. Make sure at least an option with the key 'prod' exists.", ['!field_settings' => $field_settings]) . '</p>',
  ];

  // Call submit_function() on form submission.
  $form['#submit'][] = 'users_per_environment_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Validate submission.
 */
function users_per_environment_admin_settings_validate($form, &$form_state) {
}

/**
 * Submit form data.
 */
function users_per_environment_admin_settings_submit($form, &$form_state) {
}

/**
 * Form #after_build callback for users_per_environment_admin_settings().
 */
function users_per_environment_after_build($form, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'users_per_environment') . '/css/users_per_environment.css');

  return $form;
}
