<?php

/**
 * @file
 * Provides tokens integration.
 */

/**
 * Implements hook_token_info().
 */
function users_per_environment_token_info() {
  $types = [
    'name' => t('User per Environment'),
    'description' => t('User per Environment tokens'),
  ];

  $tokens['environment_variable'] = [
    'name' => t('Environment variable'),
    'description' => t('The variable that set in settings.php file.'),
  ];

  // Return associative array of tokens & token types.
  return [
    'types' => [
      'users_per_environment' => $types,
    ],
    'tokens' => [
      'users_per_environment' => $tokens,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function users_per_environment_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // Check if token type is users_per_environment.
  if ($type == 'users_per_environment') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Set all values to users_per_environment tokens.
        case 'environment_variable':
          $replacements[$original] = users_per_environment_get_current_environment() ? users_per_environment_get_current_environment() : t('Not configured yet.');
          break;
      }
    }
  }

  return $replacements;
}
