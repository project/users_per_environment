-- SUMMARY --

Define per user on which environment they have access (dev, test, prod, etc).
When cloning back the production database to test environments test users can
then only access the test environment. It implies that the users of all
environments are created on the production environment. One place to manage all
users for all environments.

Use configuration like:
$conf['users_per_environment'] = 'dev'; 
in the settings.php. Alternatively, for autorecognition, you can use subdomains for your environments like 'test.example.com' with your production database at 'example.com'.


-- DESCRIPTION --

While code goes from a local developer machine to a staging server and then to
production, the flow for the database is usually the opposite. This to ensure
for example that any configuration changes that are not covered by Features stay
in sync and the same content node IDs from production will be used in all
environments. For example node specific templates can only be used if NIDs are
equal on all environments.

Example of the flow of code (during sprint/CI):

LOCAL -> repo feature branch -> merge to repo master branch -> deploy on DEV
server -> TST -> PROD

Back clone of the database (usually after PROD code deploy, configuration and
content addition):

PROD -> TST
PROD -> DEV
PROD -> LOCAL

The problem with this is that test users (stored in the database) would also
have access with the same role on the production environment. To avoid that
security risk this module defines per user on which environment they should have
access.

That implies that users should be created on production first. Users can still
be created on the other environments as well but it will be overwritten with the
next database clone from production.

Having environments assigned to users facilitates removing them from the
database with a query. This way the info (e.g. clients) is not available to
testers or developers. This safe database version can then be made available
through a private repo without worries.


-- CONCEPT --

Right away AFTER the login process (user must be known) an extra check is made
that passes if:

* the environment matches the one the user is allowed for
  OR
* it is the superuser logging in (uid = 1)
  OR
* in the settings the environment check is set to be skipped.

In other cases the user is logged out (user_logout()). That event is logged in
the watchdog.


-- INSTALLATION --

Before enabling the module the first time install and enable
https://www.drupal.org/project/views_bulk_operations to get a View of users per
environment at ../admin/users.

NOTA BENE:
AFTER ENABLING THE MODULE YOU STILL HAVE TO ENABLE THE ENVIRONMENTS CHECK IN
THE CONFIGURATION (SEE BELOW).

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:
  - Administer Users per Environment settings.
  - Assign Environments to individual users.

* Add Environment settings in Configuration » People » Users per Environment.

* Define your environments at
  ../admin/config/people/accounts/fields/field_users_per_environment  in the
  'Allowed values list' section. Make sure at least an option with the key
  'prod' exists.

* Add Environments for all users.

* Enable the Environments check in Configuration » People » Users per
  Environment. Initially this is disabled. For the superuser (uid = 1) the
  check is always skipped to avoid being locked out.


-- ENVIRONMENT DETECTION --

On the non-production environments (test, dev) add the following at the bottom
of the settings.php (e.g. for DEV):

$conf['users_per_environment'] = 'dev';

You can do the same on your local development machine to fake being on an
environment. In other cases there is no need to set anything on a local
development machine or on production. If no variable '$environment' in the
settings.php is defined environment detection is done based on the URL.

E.g. DEV, TEST ($env = [from the URL or settings.php]).
Get the environment part of the URL ignoring www (e.g. dev or test). This
is then used directly to know on which environment we are. We presume the
staging domain being constructed like 'www.test.example.com' (with or without
www). The possible subdomains can be configured.

If we are on a local development environment (a developers machine) or
production is determined as follows (unless defined to fake an environment):

LOCAL ($env = 'local').
Catches a development domain as being without extension e.g. 'http://localhost'
or ending with '.dev' or '.local'. No access restrictions apply on any defined
users in this case.

PROD ($env = 'prod').
We are on production if the extension of the URL (ignoring www) matches what is
set in the configuration (.com, .org, .net, .us) AND we are not on a subdomain
(usually the case on the user login page).


-- AFTER CLONING THE DATABASE --

If you want to clone back the database without production users (e.g. your
clients) you can use the included View to remove them (be cautious not to lock
yourself out).
